<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendar_events', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->date('start');
            $table->date('end');
            $table->string('status');
            $table->bigInteger('creator_id')->unsigned()->index();
            $table->foreign('creator_id')->references('id')->on('users');
            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('calendar_events', function (Blueprint $table) {
        //     $table->dropForeign('calendar_events_user_foreign');
        // });
        Schema::dropIfExists('calendar_events');
    }
};
