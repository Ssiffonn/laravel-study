<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('gender_name')->after('email')->nullable();
            $table->bigInteger('department_id')->after('gender_name')->unsigned()->index()->nullable();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('set null');
            $table->bigInteger('job_status_id')->after('department_id')->unsigned()->index()->nullable();
            $table->foreign('job_status_id')->references('id')->on('job_statuses')->onDelete('set null');
            $table->bigInteger('position_id')->after('job_status_id')->unsigned()->index()->nullable();
            $table->foreign('position_id')->references('id')->on('positions')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_department_id_foreign');
            $table->dropForeign('users_job_status_id_foreign');
            $table->dropForeign('users_position_id_foreign');

            $table->dropColumn([
                'gender_name',
                'department_id',
                'job_status_id',
                'position_id'
            ]);
        });
    }
};
