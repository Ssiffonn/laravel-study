$(document).ready(function() {
    $(document).on('click', '#events', function() {
        $('#log_timeline').hide();
        $('#event_table').show();
    })

    $(document).on('click', '#log', function() {
        $('#event_table').hide();
        $('#log_timeline').show();
    })
})