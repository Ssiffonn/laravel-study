$(document).ready(function () {
    $(document).on('click', "#addModal", function () {
        window.location.href = "users/create";
    })

    $(document).on('click', '#deleteBtn', function () {
        var table = $('#users-table').DataTable();
        var data = table.row($(this).parents('tr')).data();
        sessionStorage.setItem('id', $(this).data('id'));
    })

    $(document).on('click', "#delete", function () {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var table = $('#users-table').DataTable();
        var id = sessionStorage.getItem('id');
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax(
        {
            url: "users/" + id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success: function (data){
                $('#deleteModal').modal('hide');
                table
                    .row( $(this).parents('tr') )
                    .remove()
                    .draw();
                toastr.success(data.success, 'Успешно');
            },
            error: function (xhr) {
                $('#deleteModal').modal('hide');
                var err = jQuery.parseJSON(xhr.responseText);
                toastr.error(err.error, 'Ошибка');
            }
        });
    });

    $(document).on('click', '#editModal', function () {
        var table = $('#users-table').DataTable();
        var dataTable = table.row($(this).parents('tr')).data();
        window.location.href = "users/" + $(this).data('id') + "/edit";
    });

    $(document).on('click', '#show', function () {
        window.location.href = "users/" + $(this).data('id');
    });

    $(document).on('hide.bs.modal', '#mymodal', function(e) {
        $(this).remove();
    });

})