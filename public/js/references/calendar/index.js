$(document).ready(function () {
    toastr.options = {
        "progressBar": true,
        'timeOut': 5000
    };
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        events: {
            url: 'tasks/events',
            success: function(events) {
                events.forEach(function(item) {
                    switch(item.status) {
                        case "new":
                            item.color = 'gray';
                            break;
                        case "work":
                            item.color = 'blue';
                            break;
                        case "done":
                            item.color = 'green';
                            break;
                        default:
                            break;
                    }
                });
            },
        },
        timeZone: 'Europe/Moscow',
        headerToolbar: {
            start: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,dayGridWeek,dayGridDay,listMonth'
        },
        initialView: 'dayGridMonth',
        locale: 'ru',
        height: 720,
        selectable: true,
        editable: true,
        eventDrop: function(eventDropInfo) {
            var id = eventDropInfo.event.id;
            var title = eventDropInfo.event.title;
            var start = eventDropInfo.event.startStr;
            var end = eventDropInfo.event.endStr;
            var status = eventDropInfo.event.extendedProps.status;
            var user_id = eventDropInfo.event.extendedProps.user_id;
            var creator_id = eventDropInfo.event.extendedProps.creator_id;
            var description = eventDropInfo.event.extendedProps.description;
            sessionStorage.setItem('id', id);
            sessionStorage.setItem('title', title);
            sessionStorage.setItem('start', start);
            sessionStorage.setItem('end', end);
            sessionStorage.setItem('status', status);
            sessionStorage.setItem('user_id', user_id);
            sessionStorage.setItem('creator_id', creator_id);
            sessionStorage.setItem('description', description);
            dropEdit(eventDropInfo);
        },
        eventResize: function(eventResizeInfo) {
            var id = eventResizeInfo.event.id;
            var title = eventResizeInfo.event.title;
            var start = eventResizeInfo.event.startStr;
            var end = eventResizeInfo.event.endStr;
            var status = eventResizeInfo.event.extendedProps.status;
            var user_id = eventResizeInfo.event.extendedProps.user_id;
            var creator_id = eventResizeInfo.event.extendedProps.creator_id;
            var description = eventResizeInfo.event.extendedProps.description;
            sessionStorage.setItem('id', id);
            sessionStorage.setItem('title', title);
            sessionStorage.setItem('start', start);
            sessionStorage.setItem('end', end);
            sessionStorage.setItem('status', status);
            sessionStorage.setItem('user_id', user_id);
            sessionStorage.setItem('creator_id', creator_id);
            sessionStorage.setItem('description', description);
            dropEdit(eventResizeInfo);
        },
        select: function(selectionInfo) {
            add();
            var start = selectionInfo.startStr;
            var end = selectionInfo.endStr;
            sessionStorage.setItem('start', start);
            sessionStorage.setItem('end', end);            
        },
        eventClick: function(info) {
            var id = info.event.id;
            var title = info.event.title;
            var start = info.event.startStr;
            var end = info.event.endStr;
            var description = info.event.extendedProps.description;
            sessionStorage.setItem('id', id);
            sessionStorage.setItem('title', title);
            sessionStorage.setItem('start', start);
            sessionStorage.setItem('end', end);  
            sessionStorage.setItem('description', description);  
            edit();
        },
    });

    $(document).on('click', '#add', function() {
        var token = $("meta[name='csrf-token']").attr("content");
        var title = document.getElementById('title').value;
        var users = $('#user_id').select2("val");
        var description = document.getElementById('description').value;
        $.ajax({
            url: 'calendar',
            type: 'POST',
            data: {
                _token: token,
                title: title,
                user_id: users,
                start: sessionStorage.getItem('start'),
                end: sessionStorage.getItem('end'),
                status: 'new',
                description: description,
            },
            success: function(data) {
                calendar.addEvent({
                    id: data.id,
                    title: $('#title').val(),
                    start: sessionStorage.getItem('start'),
                    end: sessionStorage.getItem('end'),
                    color: 'gray',
                    status: 'new',
                    user_id: users,
                    description: description,
                });
                $('#modal').modal('hide');
                toastr.success(data.success, 'Успешно')
            },
            error: function(xhr) {
                var err = jQuery.parseJSON(xhr.responseText);
                var errors = err.errors;
                Object.keys(errors).forEach(key => {
                    toastr.error(errors[key], 'Ошибка');
                });
            }
        })
    })

    $(document).on('click', '#edit', function() {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var token = $("meta[name='csrf-token']").attr("content");
        var title = document.getElementById('title').value;
        var description = document.getElementById('description').value;
        var start = sessionStorage.getItem('start');
        var end = sessionStorage.getItem('end');
        var id = sessionStorage.getItem('id');
        var event = calendar.getEventById(id);
        var status = event.extendedProps.status;
        if ($('input[type=radio]:checked').length > 0) {
            status = $('input[type=radio]:checked').val();
        }
        var user_id = $('#user_id').select2("val");
        var creator_id = event.extendedProps.creator_id;
        $.ajax({
            url: 'tasks/' + id,
            type: 'PATCH',
            data: {
                _token: token,
                title: title,
                start: start,
                end: end,
                status: status,
                user_id: user_id,
                creator_id: creator_id,
                description: description,
            },
            success: function(data) {
                event.setProp('title', title);
                switch (status) {
                    case 'work':
                        event.setExtendedProp('status', 'work');
                        event.setProp('color', 'blue');
                        break;
                    case 'done':
                        event.setExtendedProp('status', 'done');
                        event.setProp('color', 'green');
                        break;
                }
                $('#modal').modal('hide');
                toastr.success(data.success, 'Успешно')
            },
            error: function(xhr) {
                var err = jQuery.parseJSON(xhr.responseText);
                var errors = err.errors;
                Object.keys(errors).forEach(key => {
                    toastr.error(errors[key], 'Ошибка');
                });
            }
        })
    })

    $(document).on('click', '#delete', function() {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var token = $("meta[name='csrf-token']").attr("content");
        var id = sessionStorage.getItem('id');
        var event = calendar.getEventById(id);
        $.ajax({
            url: 'tasks/' + id,
            type: 'DELETE',
            data: {
                _token: token,
            },
            success: function(data) {
                event.remove();
                $('#modal').modal('hide');
                toastr.success(data.success, 'Успех')
            }
        })
    })
    calendar.render();
    calendar.setOption('aspectRatio', 2);
})



$(document).on('hidden.bs.modal', '#modal', function(e) {
    $(this).remove();
});

function add() {
    $.ajax({
        url: 'tasks/create',
        success: function(data) {
            $('.wrapper').append(data);
            $('#modal').modal('show');
            $('.modal-title').html('Добавление');
            $('#btn').html('Добавить');
            $('#btn').attr('id', 'add');
            $('#user_id').select2();
        }
    })
}

function edit() {
    $.ajax({
        url: 'tasks/' + sessionStorage.getItem('id') + '/edit',
        success: function(data) {
            $('.wrapper').append(data);
            $('#modal').modal('show');
            $('.modal-title').html('Изменение');
            $('#btn').html('Изменить');
            $("#title").val(sessionStorage.getItem('title'));
            $("#description").val(sessionStorage.getItem('description'));
            $('#btn').attr('id', 'edit');
            $('#user_id').select2();
            $('#workBtn').html('Принять в работу');
            $('#workBtn').attr('id', 'work');
        }
    })
}

function dropEdit(info) {
    var token = $("meta[name='csrf-token']").attr("content");
    var title = sessionStorage.getItem('title');
    var start = sessionStorage.getItem('start');
    var end = sessionStorage.getItem('end');
    var id = sessionStorage.getItem('id');
    var status = sessionStorage.getItem('status');
    var user_id = sessionStorage.getItem('user_id');
    var creator_id = sessionStorage.getItem('creator_id');
    var description = sessionStorage.getItem('description');
    $.ajax({
        url: 'tasks/' + id,
        type: 'PATCH',
        data: {
            _token: token,
            title: title,
            start: start,
            end: end,
            status: status,
            user_id: user_id,
            creator_id: creator_id,
            description: description,
        },
        success: function(data) {
            toastr.success(data.success, 'Успешно');
        },
        error: function(xhr) {
            var err = jQuery.parseJSON(xhr.responseText);
            toastr.error(err.error, 'Ошибка');
            info.revert();
        }
    })
}