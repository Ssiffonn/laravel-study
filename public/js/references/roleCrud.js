$(document).ready(function() {
    $(document).on('click', "#addModal", function () {
        window.location.href = window.location.href + "/create";
    })
    
    $(document).on('click', '#editModal', function() {
        var table = $('#references-table').DataTable();
        var data = table.row($(this).parents('tr')).data();
        window.location.href = window.location.href + "/" + data['id'] + "/edit";
    })

    $(document).on('click', '#deleteBtn', function() {
        var table = $('#references-table').DataTable();
        var data = table.row($(this).parents('tr')).data();
        sessionStorage.setItem('id', data['id']);
    })

    $(document).on('click', '#delete', function() {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var table = $('#references-table').DataTable();
        var id = sessionStorage.getItem('id');
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
            url: window.location.href + "/" + id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success: function (data) {
                $('#deleteModal').modal('hide');
                table
                    .row( $(this).parents('tr') )
                    .remove()
                    .draw();
                toastr.success(data.message, 'Успешно');
            },
            error: function(xhr) {
                $('#deleteModal').modal('hide');
                var err = jQuery.parseJSON(xhr.responseText);
                toastr.error(err.message, 'Ошибка');
            }
        });
    })

    $(document).on('hide.bs.modal', '#mymodal', function(e) {
        $(this).remove();
    });
})