$(document).ready(function () {
    $('#addBtn').click(function () {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var data = $('#addForm').serialize();
        var table = $('#references-table').DataTable();
        $.ajax({
            url: window.location.href,
            type: "POST",
            data: data,
            success: function () {
                toastr.success('Запись успешно создана!', 'Успешно');
                $('#add').modal('hide');
                table.draw();
            },
            error: function (xhr, error, status) {
                var err = jQuery.parseJSON(xhr.responseText);
                $('.error').empty();
                var errors = err.errors;
                Object.keys(errors).forEach(key => {
                    toastr.error(errors[key], 'Ошибка');
                });
            }
        })
    })

    $(document).on('click', '#deleteBtn', function () {
        var table = $('#references-table').DataTable();
        var data = table.row($(this).parents('tr')).data();
        sessionStorage.setItem('id', data['id']);
    })

    $(document).on('click', "#delete", function () {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var table = $('#references-table').DataTable();
        var id = sessionStorage.getItem('id');
        var token = $("meta[name='csrf-token']").attr("content");
        $.ajax({
            url: window.location.href + "/" + id,
            type: 'DELETE',
            data: {
                "id": id,
                "_token": token,
            },
            success: function (data) {
                $('#deleteModal').modal('hide');
                table
                    .row( $(this).parents('tr') )
                    .remove()
                    .draw();
                toastr.success(data.message, 'Успешно');
            },
            error: function(xhr) {
                $('#deleteModal').modal('hide');
                var err = jQuery.parseJSON(xhr.responseText);
                toastr.error(err.message, 'Ошибка');
            }
        });
    });

    $(document).on('click', '#editModal', function () {
        var table = $('#references-table').DataTable();
        var data = table.row($(this).parents('tr')).data();
        $('#editName').val(data['name']);
        sessionStorage.setItem('id', data['id']);
    });

    $('#editBtn').click(function () {
        toastr.options = {
            "progressBar": true,
            'timeOut': 5000
        };
        var inputData = $('#editForm').serialize();
        var table = $('#references-table').DataTable();
        $.ajax({
            url: window.location.href + '/' + sessionStorage.getItem('id'),
            type: "PATCH",
            data: inputData,
            success: function () {
                $('#modal').modal('hide');
                toastr.success('Запись успешно обнавлена!', 'Успешно');
                table.draw();
            },
            error: function (xhr, error, status) {
                var err = jQuery.parseJSON(xhr.responseText);
                $('.error').empty();
                var errors = err.errors;
                Object.keys(errors).forEach(key => {
                    toastr.error(errors[key], 'Ошибка');
                });
            }
        })
    })

    $('#add').on('hide.bs.modal', function(e) {
        $('#add form')[0].reset();
        $('.error').empty();
    })
    $('#modal').on('hide.bs.modal', function(e) {
        $('#modal form')[0].reset();
        $('.error').empty();
    })
})