<?php

return [
    'add' => 'Создать статью',
    'addTitle' => 'Добавление новой статьи',
    'edit' => 'Редактировать статью',
    'editTitle' => 'Редактирование: ',
    'success' => 'Ваша статья успешно создана!',
    'blog' => 'Главная',
    'articles' => 'Статьи',
    'latest' => 'Последняя статья: ',
    'published' => 'Опубликованно: :published',
    'tags' => 'Теги: ',
    'creator' => 'Создатель: :creator',
    'mainTitle' => 'Статьи',
    'formTitle' => ':title :article',
    'title' => 'Заголовок: ',
    'body' => 'Содержимое: ',
    'publishAt' => 'Опубликовать',
    'full' => 'Показать полностью'
];
