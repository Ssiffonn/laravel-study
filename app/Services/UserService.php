<?php

namespace App\Services;

use App\Models\Department;
use App\Models\JobStatus;
use App\Models\Position;
use App\Models\Role;
use App\Models\User;
use App\Models\UserLogActivity;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;
use Spatie\Activitylog\Models\Activity;

// use Spatie\Activitylog\Models\Activity;

class UserService
{
    /**
     * Главная страница со всеми пользователями
     *
     * @param  \App\DataTables\UsersDataTable $dataTable
     */
    public function index($dataTable)
    {
        if (!Auth::user()->isAbleTo('users-create|users-update|users-delete')) {
            return redirect()->route('users.show', ['user' => Auth::user()->id]);
        }
        return $dataTable->render('users.index');
    }

    /**
     * Страница создания пользователя
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $departments = Department::all();
        $positions = Position::all();
        $jobStatuses = JobStatus::all();
        $roles = Role::all();
        return view('users.form')->with([
            'departments' => $departments,
            'positions' => $positions,
            'jobStatuses' => $jobStatuses,
            'roles' => $roles,
            'title' => __('user.add'),
            'btn' => __('user.add'),
            'route' => route('users.store')
        ]);
    }

    /**
     * Сохраняет пользователя в БД
     *
     * @param  array<string, mixed. $data
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($data): RedirectResponse
    {
        $role = Role::find($data['role']);
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        $user->attachRole($data['role']);
        $data = $this->getData($data, $user);
        $data['role'] = $role->display_name;
        $data = collect($data);
        activity()
            ->by(Auth::user())
            ->on($user)
            ->withProperties(['attributes' => $data->only(User::ACCEPT)])
            ->event('created')
            ->log('created');

        toastr()->success('Запись успешо создана!', 'Успех');
        return redirect()->route('users.index');
    }

    public function show($user, $dataTable)
    {
        $activity = Activity::when(!$user->hasRole('admin'), function ($query) use ($user) {
            $query->where('subject_id', '=', $user->id)->orWhere('causer_id', '=', $user->id);
        })->orderBy('id', 'desc')->get();
        return $dataTable->render('users.show', [
            'user' => $user,
            'activity' => $activity,
        ]);
    }

    /**
     * Страница редактирования пльзователя
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\View\View
     */
    public function edit($user): View
    {
        $departments = Department::all();
        $positions = Position::all();
        $jobStatuses = JobStatus::all();
        $roles = Role::all();
        return view('users.form')->with([
            'user' => $user,
            'departments' => $departments,
            'positions' => $positions,
            'jobStatuses' => $jobStatuses,
            'roles' => $roles,
            'title' => __('user.edit'),
            'btn' => __('user.edit'),
            'route' => route('users.update', $user->id),
            'method' => 'PATCH',
        ]);
    }

    /**
     * Обновляет данные пользователя в БД
     *
     * @param  array<string, mixed> $data
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($data, $user): RedirectResponse
    {
        $oldRole = $user->roles[0]->display_name;
        $newRole = Role::find($data['role']);
        $attributes = [];
        $oldData = [];
        $oldUser = $user->replicate();
        if ($data['password'] != null) {
            $data['password'] = Hash::make($data['password']);
        } else {
            $data['password'] = $user->password;
        }
        $user->fill($data);
        $attributes = $user->getDirty();
        if ($oldRole != $newRole->display_name) {
            $oldData['role'] = $oldRole;
            $attributes['role'] = $newRole->display_name;
        }
        foreach ($oldUser->toArray() as $key => $row) {
            if ($user->isDirty($key)) {
                $oldData[$key] = $row;
            }
        }
        $oldData = $this->getData($oldData, $oldUser);
        $attributes = $this->getData($attributes, $user);

        $old = collect($oldData);
        $new = collect($attributes);
        $user->save();
        activity()
            ->by(Auth::user())
            ->on($user)
            ->withProperties([
                'old' => $old->only(User::ACCEPT),
                'attributes' => $new->only(User::ACCEPT)
            ])
            ->event('updated')
            ->log('updated');

        $user->syncRoles([$data['role']]);
        toastr()->success('Запись успешо обновлена!', 'Успех');
        return redirect()->route('users.index');
    }

    /**
     * Удаление данных из бд
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $user = User::find($id);
        if ($user != Auth::user() && !$user->hasRole('admin')) {
            $role = $user->roles()->first();
            $data = $this->getData($user->toArray(), $user);
            $data['role'] = $role->display_name;
            $data = collect($data);
            activity()
                ->by(Auth::user())
                ->on($user)
                ->withProperties(['attributes' => $data->only(User::ACCEPT)])
                ->event('deleted')
                ->log('deleted');

            $user->detachRole($role->name);
            User::destroy($id);
            return response()->json([
                'success' => 'Запись успешно удалена!'
            ], 200);
        } else {
            return response()->json([
                'error' => 'Нельзя удалить!'
            ], 403);
        }
    }

    /**
     * Возвращает массив данных для записи в лог
     *
     * @param  array $data
     * @param  \App\Models\User $user
     * @return array
     */
    private function getData($data, $user): array
    {
        foreach ($data as $key => $row) {
            if (array_key_exists($key, User::RELATIONS)) {
                if ($row != null) {
                    $tmp = User::RELATIONS[$key];
                    $data[$key] = $user->$tmp->name;
                } else {
                    $data[$key] = __('user.undefined');
                }
            } else {
                $data [$key] = $row;
            }
        }
        return $data;
    }
}
