<?php

namespace App\Services;

use App\Models\Article;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class ArticleService
{
    /**
     * Возвращает главную страницу со всеми статьями
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index($request): View
    {
        $articles = Article::published()->when($request->has('tag'), function ($query) use ($request) {
            $query->whereHas('tags', function ($query) use ($request) {
                $query->where('name', $request['tag']);
            });
        })->get();
        return view('articles.index')->with(['articles' => $articles]);
    }

    /**
     * Возвращает страницу с деталями статьи
     *
     * @param  \App\Models\Article $article
     * @return \Illuminate\Contracts\View\View
     */
    public function show($article): View
    {
        $loggedUser = Auth::id();
        $article = Article::published()->findOrFail($article->id);
        return view('articles.show')->with(['article' => $article, 'loggedUser' => $loggedUser]);
    }

    /**
     * Возвращает страницу создания статьи
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create(): View
    {
        $tags = Tag::all();
        return view('articles.form')->with([
            'published_at' => Carbon::now(),
            'tags' => $tags,
            'submitButtonText' => __('article.add'),
            'route' => route('articles.store'),
            'title' => __('article.addTitle'),
        ]);
    }

    /**
     * Сохраняет статью в базу данных
     *
     * @param array<string, mixed> $data
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($data): RedirectResponse
    {
        $article = Auth::user()->articles()->create($data);
        $article->tags()->sync($data['tags']);
        toastr()->success('Статья успешно создана!', 'Успех');
        return redirect('articles');
    }

    /**
     * Возвращает страницу для редактирования статьи
     *
     * @param  \App\Models\Article $article
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($article): View
    {
        $loggedUser = Auth::id();
        $user = Auth::user();
        if ($loggedUser != $article->user_id && !$user->hasRole('admin')) {
            abort('403');
        }
        $tags = Tag::all();
        return view('articles.form')->with([
            'article' => $article,
            'tags' => $tags,
            'submitButtonText' => __('article.edit'),
            'route' => route('articles.update', $article->id),
            'method' => 'PATCH',
            'title' => __('article.editTitle'),
            'published_at' => Carbon::createFromFormat('d.m.Y H:i', $article->published_at),
        ]);
    }

    /**
     * Обнавляет статью в базе данных
     *
     * @param array<string, mixed> $data
     * @param  \App\Models\Article $article
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($data, $article): RedirectResponse
    {
        $article->update($data);
        $article->tags()->sync($data['tags']);
        toastr()->success('Статья успешно обновлена!', 'Успех');
        return redirect(route('articles.index'));
    }
}
