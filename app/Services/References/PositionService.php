<?php

namespace App\Services\References;

use App\Models\Position;
use Illuminate\Http\JsonResponse;

class PositionService
{
    /**
     * Показывает все статусы
     *
     * @param  App\DataTables\References\PositionsDataTable $dataTable
     * @return void
     */
    public function index($dataTable)
    {
        return $dataTable->render('catalogs.positions.index');
    }

    /**
     * Сохраняет данные в БД
     *
     * @param  array $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($request): JsonResponse
    {
        Position::create($request);
        return response()->json([
            'success' => 'Record created successfully!'
        ]);
    }

    /**
     * Удаляет данные из БД
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $position = Position::find($id);
        if ($position->users()->exists()) {
            return response()->json([
                "message" => "Есть работники с такой должностью! Нельзя удалить!"
            ], 403);
        } else {
            Position::destroy($id);
            return response()->json([
                "message" => "Данные успешно удалены!"
            ], 200);
        }
    }

    /**
     * Обновляет данные в БД
     *
     * @param  array $data
     * @param  \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($data, $position): JsonResponse
    {
        $position->update($data);
        return response()->json([
            'success' => 'Record updated successfully!'
        ]);
    }
}
