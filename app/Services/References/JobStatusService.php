<?php

namespace App\Services\References;

use App\Models\JobStatus;
use Illuminate\Http\JsonResponse;

class JobStatusService
{
    /**
     * Показывает все статусы
     *
     * @param \App\DataTables\References\JobStatusesDataTable $dataTable
     * @return void
     */
    public function index($dataTable)
    {
        return $dataTable->render('catalogs.job_statuses.index');
    }

    /**
     * Сохраняет данные в БД
     *
     * @param  array $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($request): JsonResponse
    {
        JobStatus::create($request);
        return response()->json([
            'success' => 'Record created successfully!'
        ]);
    }

    /**
     * Удаляет данные из БД
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $status = JobStatus::find($id);
        if ($status->users()->exists()) {
            return response()->json([
                "message" => "Есть работники с таким статусом! Нельзя удалить!"
            ], 403);
        } else {
            JobStatus::destroy($id);
            return response()->json([
                "message" => "Данные успешно удалены!"
            ], 200);
        }
    }

    /**
     * update
     *
     * @param  array $data
     * @param  \App\Models\JobStatus $jobStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($data, $jobStatus): JsonResponse
    {
        $jobStatus->update($data);
        return response()->json([
            'success' => 'Record updated successfully!'
        ]);
    }
}
