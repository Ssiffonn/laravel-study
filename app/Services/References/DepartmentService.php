<?php

namespace App\Services\References;

use App\Models\Department;
use Illuminate\Http\JsonResponse;

class DepartmentService
{
    /**
     * Показывает все отделы
     *
     * @param \App\DataTables\References\DepartmentsDataTable $dataTable
     * @return void
     */
    public function index($dataTable)
    {
        return $dataTable->render('catalogs.departments.index');
    }

    /**
     * Запись отдела в БД
     *
     * @param  \App\Http\Requests\DepartmentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($request): JsonResponse
    {
        Department::create($request);
        // $this->test();
        return response()->json([
            'success' => 'Record created successfully!',
        ]);
    }

    // public function test()
    // {
    //     toastr()->error('asdasd');
    //     return redirect()->route('departments.index');
    // }

    /**
     * Удаляет отдел из БД
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $department = Department::find($id);
        if ($department->users()->exists()) {
            return response()->json([
                "message" => "В отделе есть работники! Нельзя удалить!"
            ], 403);
        } else {
            Department::destroy($id);
            return response()->json([
                "message" => "Данные успешно удалены!"
            ], 200);
        }
    }

    /**
     * Обновляет данные в БД
     *
     * @param  array $data
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($data, $department): JsonResponse
    {
        $department->update($data);
        return response()->json([
            'success' => 'Record updated successfully!'
        ]);
    }
}
