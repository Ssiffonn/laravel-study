<?php

namespace App\Services\References;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\View\View;

class RoleService
{
    /**
     * Показывает все роли
     *
     * @param  \App\DataTables\References\RoleDataTable $dataTable
     */
    public function index($dataTable)
    {
        return $dataTable->render('catalogs.roles.index');
    }

    /**
     * Показывает форму создания роли
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $permissions = Config::get('laratrust_seeder.ru_perm');
        return view('catalogs.roles.form')->with([
            'permissions' => $permissions,
            'title' => 'Добавление',
            'btn' => 'Добавить',
            'route' => route('roles.store'),
        ]);
    }

    /**
     * Заносит данные в БД
     *
     * @param  \App\Http\Requests\RoleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store($request): RedirectResponse
    {
        $role = Role::create([
            'name' => $request['name'],
            'display_name' => $request['display_name'],
        ]);
        $role->attachPermissions($request['perm']);
        toastr()->success('Данные успешно созданы!', 'Успешно!');
        return redirect()->route('roles.index');
    }

    /**
     * Выводит форму редактирования роли
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\View\View
     */
    public function edit($role): View
    {
        $permissions = Config::get('laratrust_seeder.ru_perm');
        return view('catalogs.roles.form')->with([
            'permissions' => $permissions,
            'role' => $role,
            'title' => 'Изменение',
            'btn' => 'Изменить',
            'method' => 'PATCH',
            'route' => route('roles.update', $role->id),
        ]);
    }

    /**
     * Обновляет данные из БД
     *
     * @param  \App\Http\Requests\RoleRequest  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($request, $role): RedirectResponse
    {
        $role->update([
            'name' => $request['name'],
            'display_name' => $request['display_name'],
        ]);
        $role->syncPermissions($request['perm']);
        toastr()->success('Данные успешно изменены!', 'Успешно!');
        return redirect()->route('roles.index');
    }

    /**
     * Удаляет данные из БД
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $role = Role::find($id);
        if ($role->users()->exists()) {
            return response()->json([
                "message" => "Есть раобники с такой ролью! Нельзя удалить!"
            ], 403);
        } else {
            Role::destroy($id);
            return response()->json([
                "message" => "Данные успешно удалены!"
            ], 200);
        }
    }
}
