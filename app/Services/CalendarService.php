<?php

namespace App\Services;

use App\Models\Calendar;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CalendarService
{
    /**
     * Выводит календарь со всеми ивентами
     *
     * @return \Illuminate\View\View
     */
    public function index(): View
    {
        return view('catalogs.calendar.index');
    }

    /**
     * Выводит форму для создания ивента
     *
     * @return \Illuminate\View\View
     */
    public function create(): View
    {
        $role = Role::select('name')->where('name', '!=', 'admin')->get();
        $users = User::whereRoleIs($role->toArray())->get();
        return view('catalogs.calendar.form')->with([
            'users' => $users,
        ]);
    }

    /**
     * Сохраняет ивент в бд
     *
     * @param  array $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($request): JsonResponse
    {
        $calendar = Calendar::create($request);
        return response()->json([
            'success' => 'Событие успешно создано!',
            'id' => $calendar->id,
        ]);
    }

    /**
     * Выводит форму редактирования ивента
     *
     * @param  int $id
     * @return \Illuminate\View\View
     */
    public function edit($id): View
    {
        $event = Calendar::find($id);
        $role = Role::select('name')->where('name', '!=', 'admin')->get();
        $users = User::whereRoleIs($role->toArray())->get();
        return view('catalogs.calendar.form')->with([
            'users' => $users,
            'event' => $event,
            'model' => App\Models\Calendar::class,
        ]);
    }

    /**
     * Обновляет данные в БД
     *
     * @param  array $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($request, $id): JsonResponse
    {
        $event = Calendar::find($id);
        if ($request['start'] != $event['start'] || $request['end'] != $event['end']) {
            if (Auth::user()->id == $event['creator_id'] || Auth::user()->hasRole('admin')) {
                $event->update($request);
                return response()->json([
                    'success' => 'Событие было успешно обновлено!',
                ]);
            } else {
                return response()->json([
                    'error' => 'Нельзя изменить событие!',
                ], 403);
            }
        } else {
            $event->update($request);
            return response()->json([
                'success' => 'Событие было успешно обновлено!',
            ]);
        }
    }

    /**
     * Удаляет данные из БД
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        Calendar::destroy($id);
        return response()->json([
            'success' => 'Событие успешно удалено!'
        ]);
    }

    /**
     * Подгружает задачи по времени
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function events($request): JsonResponse
    {
        $events = Calendar::when(!Auth::user()->hasRole('admin'), function ($query) {
            $query->where('user_id', Auth::user()->id)->orWhere('creator_id', Auth::user()->id);
        })->where('start', '>=', $request['start'])->where('end', '<=', $request['end'])->get();
        return response()->json($events, 200);
    }
}
