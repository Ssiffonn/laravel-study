<?php

namespace App\Observers;

use App\Models\Calendar;
use Illuminate\Support\Facades\Auth;

class CalendarObserver
{
    /**
     * Handle the Calendar "creating" event.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return void
     */
    public function creating(Calendar $calendar)
    {
        $calendar->creator_id = Auth::user()->id;
    }

    /**
     * Handle the Calendar "updated" event.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return void
     */
    public function updated(Calendar $calendar)
    {
        //
    }

    /**
     * Handle the Calendar "deleted" event.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return void
     */
    public function deleted(Calendar $calendar)
    {
        //
    }

    /**
     * Handle the Calendar "restored" event.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return void
     */
    public function restored(Calendar $calendar)
    {
        //
    }

    /**
     * Handle the Calendar "force deleted" event.
     *
     * @param  \App\Models\Calendar  $calendar
     * @return void
     */
    public function forceDeleted(Calendar $calendar)
    {
        //
    }
}
