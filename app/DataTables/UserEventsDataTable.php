<?php

namespace App\DataTables;

use App\Models\Calendar;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UserEventsDataTable extends DataTable
{
    public function __construct($user)
    {
        $this->user = $user;
    }
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
        ->editColumn('start', function (Calendar $calendar) {
            return Carbon::parse($calendar->start)->format('d.m.Y');
        })
        ->editColumn('end', function (Calendar $calendar) {
            return Carbon::parse($calendar->end)->format('d.m.Y');
        })
        ->editColumn('creator_id', function (Calendar $calendar) {
            return $calendar->creator->name;
        })
        ->editColumn('user_id', function (Calendar $calendar) {
            return $calendar->user->name;
        })
        ->editColumn('status', function (Calendar $calendar) {
            return Calendar::STATUS[$calendar->status];
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \Calendar $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Calendar $model): QueryBuilder
    {
        return $model->when(!$this->user->hasRole('admin'), function ($query) {
            $query->where('user_id', $this->user->id)->orWhere('creator_id', $this->user->id);
        });
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('userevents-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lfrtip');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            'id' => [
                'title' => 'ID',
                'name' => 'id',
                'data' => 'id'
            ],
            'title' => [
                'title' => 'Название события',
                'name' => 'title',
                'data' => 'title'
            ],
            'description' => [
                'title' => 'Описание события',
                'name' => 'description',
                'data' => 'description'
            ],
            'status' => [
                'title' => 'Статус',
                'name' => 'status',
                'data' => 'status'
            ],
            'start' => [
                'title' => 'Начало события',
                'name' => 'start',
                'data' => 'start'
            ],
            'end' => [
                'title' => 'Конец события',
                'name' => 'end',
                'data' => 'end'
            ],
            'creator_id' => [
                'title' => 'Создатель',
                'name' => 'creator_id',
                'data' => 'creator_id'
            ],
            'user_id' => [
                'title' => 'Ответственный',
                'name' => 'user_id',
                'data' => 'user_id'
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'UserEvents_' . date('YmdHis');
    }
}
