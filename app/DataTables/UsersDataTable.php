<?php

namespace App\DataTables;

use App\Models\Gender;
use App\Models\JobStatus;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->editColumn('user_created_at', function (User $user) {
                return Carbon::parse($user->user_created_at)->format('d.m.Y');
            })
            ->editColumn('action', function (User $user) {
                if (!$user->hasRole('admin')) {
                    return view('buttons.user_buttons')->with([
                        'id' => $user->id,
                        'model' => App\Models\User::class
                    ]);
                }
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model): QueryBuilder
    {
        return $model
            ->select([
                'users.id',
                'users.name as user_name',
                'users.email as user_email',
                'roles.display_name as role_name',
                'departments.name as department_name',
                'positions.name as position_name',
                'job_statuses.name as status_name',
                'users.gender_name as user_gender',
                'users.created_at as user_created_at',
            ])
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->leftJoin('departments', 'users.department_id', '=', 'departments.id')
            ->leftJoin('positions', 'users.position_id', '=', 'positions.id')
            ->leftJoin('job_statuses', 'users.Job_status_id', '=', 'job_statuses.id');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('users-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lfrtip')
                    ->orderBy(1, 'asc');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            'id' => [
                'title' => 'ID',
                'name' => 'users.id'
            ],
            'user_name' => [
                'title' => 'Имя',
                'name' => 'users.name',
            ],
            'user_email' => [
                'title' => 'E-mail',
                'name' => 'email',
            ],
            'role_name' => [
                'title' => 'Роль',
                'name' => 'roles.display_name',
            ],
            'department_name' => [
                'title' => 'Отдел',
                'name' => 'departments.name',
                'defaultContent' => 'Не выбрано'
            ],
            'position_name' => [
                'title' => 'Должность',
                'name' => 'positions.name',
                'defaultContent' => 'Не выбрано'
            ],
            'status_name' => [
                'title' => 'Статус',
                'name' => 'job_statuses.name',
                'defaultContent' => 'Не выбрано'
            ],
            'user_gender' => [
                'title' => 'Пол',
                'name' => 'users.gender_name'
            ],
            'user_created_at' => [
                'title' => 'Создан',
                'name' => 'users.created_at',
            ],
            'action' => [
                'title' => '',
                'name' => 'action',
                'searchable' => false,
                'orderable' => false
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Users_' . date('YmdHis');
    }
}
