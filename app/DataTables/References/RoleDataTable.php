<?php

namespace App\DataTables\References;

use App\Models\Role;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class RoleDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', function (Role $role) {
                if ($role->name != 'admin') {
                    return view('buttons.buttons')->with([
                        'id' => $role->id,
                        'model' => App\Models\Role::class
                    ]);
                }
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Role $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Role $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('references-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lfrtip');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            'id' => [
                'title' => 'id',
                'name' => 'id',
                'data' => 'id'
            ],
            'name' => [
                'title' => 'Название роли',
                'name' => 'name',
                'data' => 'name'
            ],
            'display_name' => [
                'title' => 'Отображаемое название роли',
                'name' => 'name',
                'data' => 'display_name'
            ],
            'created_at' => [
                'title' => 'Дата создания',
                'name' => 'created_at',
                'data' => 'created_at'
            ],
            'action' => [
                'title' => '',
                'name' => 'action',
                'searchable' => false,
                'orderable' => false
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Role_' . date('YmdHis');
    }
}
