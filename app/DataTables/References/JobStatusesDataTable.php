<?php

namespace App\DataTables\References;

use App\Models\JobStatus;
use Illuminate\Database\Eloquent\Builder as QueryBuilder;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class JobStatusesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param QueryBuilder $query Results from query() method.
     * @return \Yajra\DataTables\EloquentDataTable
     */
    public function dataTable(QueryBuilder $query): EloquentDataTable
    {
        return (new EloquentDataTable($query))
            ->addColumn('action', function (JobStatus $status) {
                return view('buttons.buttons')->with([
                    'id' => $status->id,
                    'model' => App\Models\JobStatus::class,
                ]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\JobStatus $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(JobStatus $model): QueryBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html(): HtmlBuilder
    {
        return $this->builder()
                    ->setTableId('references-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('lfrtip');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            'id' => [
                'title' => 'id',
                'name' => 'id',
                'data' => 'id'
            ],
            'name' => [
                'title' => 'Название статуса',
                'name' => 'name',
                'data' => 'name'
            ],
            'created_at' => [
                'title' => 'Дата создания',
                'name' => 'created_at',
                'data' => 'created_at'
            ],
            'action' => [
                'title' => '',
                'name' => 'action',
                'searchable' => false,
                'orderable' => false
            ]
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'JobStatuses_' . date('YmdHis');
    }
}
