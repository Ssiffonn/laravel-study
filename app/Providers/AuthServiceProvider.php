<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;

use App\Models\Article;
use App\Models\Calendar;
use App\Models\Department;
use App\Models\JobStatus;
use App\Models\Position;
use App\Models\Role;
use App\Models\User;
use App\Policies\ArticlePolicy;
use App\Policies\CalendarPolicy;
use App\Policies\DepartmentPolicy;
use App\Policies\JobStatusPolicy;
use App\Policies\PositionPolicy;
use App\Policies\RolePolicy;
use App\Policies\UserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Article::class => ArticlePolicy::class,
        Department::class => DepartmentPolicy::class,
        JobStatus::class => JobStatusPolicy::class,
        Position::class => PositionPolicy::class,
        Role::class => RolePolicy::class,
        User::class => UserPolicy::class,
        Calendar::class => CalendarPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('articles-read', function (User $user) {
            return $user->hasPermission('articles-read');
        });
        Gate::define('departments-read', function (User $user) {
            return $user->hasPermission('departments-read');
        });
        Gate::define('job_statuses-read', function (User $user) {
            return $user->hasPermission('job_statuses-read');
        });
        Gate::define('positions-read', function (User $user) {
            return $user->hasPermission('positions-read');
        });
        Gate::define('roles-read', function (User $user) {
            return $user->hasPermission('roles-read');
        });
        Gate::define('users-read', function (User $user) {
            return $user->hasPermission('users-read');
        });
        Gate::define('calendar-read', function (User $user) {
            return $user->hasPermission('calendar-read');
        });
    }
}
