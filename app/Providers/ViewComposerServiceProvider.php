<?php

namespace App\Providers;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->composerNavigation();
    }

    /**
     * composerNavigation
     *
     * @return \Illuminate\Contracts\View\View
     */
    private function composerNavigation()
    {
        view()->composer('partials.nav', function ($view) {
            $view->with('latest', Article::where('published_at', '<=', Carbon::now())
            ->latest('published_at')->first());
        });
    }
}
