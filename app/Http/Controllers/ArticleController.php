<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use App\Services\ArticleService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Создание контроллера
     *
     * @param \App\Services\ArticleService $service
     * @return void
     */
    public function __construct(ArticleService $service)
    {
        $this->service = $service;
    }

    /**
     * Главная страница со всеми статьями
     *
     * @param Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request): View
    {
        return $this->service->index($request);
    }

    /**
     * Просмотр отдельной статьи
     *
     * @param \App\Models\Article $article Модель статьи
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Article $article): View
    {
        return $this->service->show($article);
    }

    /**
     * Страница создания статьи
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create(): View
    {
        return $this->service->create();
    }

    /**
     * Сохранение статью в базу данных
     *
     * @param \App\Http\Requests\ArticleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ArticleRequest $request): RedirectResponse
    {
        return $this->service->store($request->validated());
    }

    /**
     * Страница редактирования статьи
     *
     * @param \App\Models\Article $article Модель статьи
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Article $article): View
    {
        return $this->service->edit($article);
    }

    /**
     * Сохранение отредактированной статьи в базу данных
     *
     * @param \App\Http\Requests\ArticleRequest $request
     * @param \App\Models\Article $article Модель статьи
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ArticleRequest $request, Article $article): RedirectResponse
    {
        return $this->service->update($request->validated(), $article);
    }
}
