<?php

namespace App\Http\Controllers\References;

use App\DataTables\References\JobStatusesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\JobStatusRequest;
use App\Models\JobStatus;
use App\Services\References\JobStatusService;
use Illuminate\Http\JsonResponse;

class JobStatusController extends Controller
{
    public function __construct(JobStatusService $service)
    {
        $this->service = $service;
        $this->authorizeResource(JobStatus::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @param \App\DataTables\JobStatusesDataTable $dataTable
     * @return void
     */
    public function index(JobStatusesDataTable $dataTable)
    {
        return $this->service->index($dataTable);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\JobStatusRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(JobStatusRequest $request): JsonResponse
    {
        return $this->service->store($request->validated());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\JobStatusRequest  $request
     * @param  \App\Models\JobStatus  $jobStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(JobStatusRequest $request, JobStatus $jobStatus)
    {
        return $this->service->update($request->validated(), $jobStatus);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobStatus  $jobStatus
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(JobStatus $jobStatus): JsonResponse
    {
        return $this->service->destroy($jobStatus->id);
    }
}
