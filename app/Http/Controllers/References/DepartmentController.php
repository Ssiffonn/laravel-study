<?php

namespace App\Http\Controllers\References;

use App\DataTables\References\DepartmentsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentRequest;
use App\Models\Department;
use App\Services\References\DepartmentService;
use Illuminate\Http\JsonResponse;

class DepartmentController extends Controller
{
    public function __construct(DepartmentService $service)
    {
        $this->service = $service;
        $this->authorizeResource(Department::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\DataTables\DepartmentsDataTable $dataTable
     * @return void
     */
    public function index(DepartmentsDataTable $dataTable)
    {
        return $this->service->index($dataTable);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DepartmentRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DepartmentRequest $request): JsonResponse
    {
        return $this->service->store($request->validated());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Department  $department
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(DepartmentRequest $request, Department $department): JsonResponse
    {
        return $this->service->update($request->validated(), $department);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Department $department
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Department $department): JsonResponse
    {
        return $this->service->destroy($department->id);
    }
}
