<?php

namespace App\Http\Controllers\References;

use App\DataTables\References\PositionsDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\PositionRequest;
use App\Models\Position;
use App\Services\References\PositionService;
use Illuminate\Http\JsonResponse;

class PositionController extends Controller
{
    public function __construct(PositionService $service)
    {
        $this->service = $service;
        $this->authorizeResource(Position::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @param App\DataTables\PositionsDataTable $dataTable
     * @return void
     */
    public function index(PositionsDataTable $dataTable)
    {
        return $this->service->index($dataTable);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PositionRequest $request): JsonResponse
    {
        return $this->service->store($request->validated());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PositionRequest  $request
     * @param  \App\Models\Position  $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PositionRequest $request, Position $position): JsonResponse
    {
        return $this->service->update($request->validated(), $position);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Position $position): JsonResponse
    {
        return $this->service->destroy($position->id);
    }
}
