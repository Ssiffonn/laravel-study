<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string|min:3|max:255|unique:job_statuses,name,NULL,id,deleted_at,NULL',
                ];
                break;
            case 'PATCH':
                return [
                    'name' => 'required|string|min:3|max:255|unique:job_statuses,name,' . $this->job_status->id
                    . ',id,deleted_at,NULL',
                ];
                break;
            default:
                break;
        }
    }
}
