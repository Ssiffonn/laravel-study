<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string|unique:users,name,NULL,id,deleted_at,NULL',
                    'email' => 'required|email|unique:users,email',
                    'password' => 'required|string|min:8|confirmed',
                    'password_confirmation' => 'required|same:password',
                    'department_id' => 'nullable|int',
                    'position_id' => 'nullable|int',
                    'gender_name' => 'required|string',
                    'job_status_id' => 'nullable|int',
                    'role' => 'required|int'
                ];
                break;
            case 'PATCH':
                return [
                    'name' => 'required|string|unique:users,name,' . $this->user->id
                    . ',id,deleted_at,NULL',
                    'email' => 'required|email|unique:users,email,' . $this->user->id,
                    'password' => 'nullable|string|min:8|confirmed',
                    'password_confirmation' => 'nullable|same:password',
                    'department_id' => 'nullable|int',
                    'position_id' => 'nullable|int',
                    'gender_name' => 'required|string',
                    'job_status_id' => 'nullable|int',
                    'role' => 'required|int'
                ];
                break;
            default:
                break;
        }
    }
}
