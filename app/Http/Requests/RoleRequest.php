<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'name' => 'required|string|min:3|max:255|unique:roles,name',
                    'display_name' => 'required|string|min:3|max:255|unique:roles,display_name',
                    'description' => 'nullable|string|min:3|max:255',
                    'perm' => 'required|array',
                ];
                break;
            case 'PATCH':
                return [
                    'name' => 'required|string|min:3|max:255|unique:roles,name,' . $this->role->id,
                    'display_name' => 'required|string|min:3|max:255|unique:roles,display_name,' . $this->role->id,
                    'description' => 'nullable|string|min:3|max:255',
                    'perm' => 'required|array',
                ];
                break;
            default:
                break;
        }
    }
}
