<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Calendar extends Model
{
    use HasFactory;

    public const STATUS = [
        'new' => 'Новая задача',
        'work' => 'В работе',
        'done' => 'Выполнена',
    ];

    protected $fillable = [
        'title',
        'description',
        'start',
        'end',
        'status',
        'creator_id',
        'user_id',
    ];

    protected $table = 'calendar_events';

    /**
     * Отношение для вывода имени создателя
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    /**
     * Отношение для вывода имени пользователя
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
