<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Laratrust\Traits\LaratrustUserTrait;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use HasApiTokens;
    use HasFactory;
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'gender_name',
        'department_id',
        'job_status_id',
        'position_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public const GENDER = [
        'male' => [
            'name' => 'Мужчина',
            'id' => 1
        ],
        'female' => [
            'name' => 'Женщина',
            'id' => 2
        ]
    ];

    public const EVENT_NAME = [
        'created' => 'создал',
        'updated' => 'обновил',
        'deleted' => 'удалил'
    ];

    public const COLUMN = [
        'name' => 'Имя',
        'email' => 'E-mail',
        'gender_name' => 'Пол',
        'department_id' => 'Отдел',
        'job_status_id' => 'Статус',
        'position_id' => 'Должность',
        'role' => 'Роль'
    ];

    public const RELATIONS = [
        'department_id' => 'department',
        'job_status_id' => 'jobStatus',
        'position_id' => 'position'
    ];

    public const ACCEPT = [
        'name',
        'email',
        'gender_name',
        'department_id',
        'job_status_id',
        'position_id',
        'role'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Мутатор для даты
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>Carbon::parse($value)->format('d.m.Y'),
        );
    }

    /**
     * Мутатор для даты
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function updatedAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>Carbon::parse($value)->format('d.m.Y'),
        );
    }

    /**
     * Отношение со статьями
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    /**
     * Отношение с отделами
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department(): BelongsTo
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * Отношение со статусами
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jobStatus(): BelongsTo
    {
        return $this->belongsTo(JobStatus::class);
    }

    /**
     * Отношение с должностями
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function position(): BelongsTo
    {
        return $this->belongsTo(Position::class);
    }
}
