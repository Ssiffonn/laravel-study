<?php

namespace App\Models;

use App\Models\Scopes\LatestScope;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Article extends Model
{
    use HasFactory;

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    public static function booted()
    {
        static::addGlobalScope(new LatestScope());
    }

    /**
     * Для массового назначения
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'body',
        'published_at',
        'user_id',
    ];

    /**
     * Столбцы для Carbon
     *
     * @var array<int, string>
     */
    protected $dates = [
        'published_at'
    ];

    /**
     * Мутатор для даты
     *
     * @return \Illuminate\Database\Eloquent\Casts\Attribute
     */
    protected function publishedAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>Carbon::parse($value)->format('d.m.Y H:i'),
            set: fn ($value) =>Carbon::createFromFormat('d.m.Y H:i', $value)
        );
    }

    /**
     * Локальный скоуп для отображения уже опубликованных статей
     *
     * @param $query
     */
    public function scopePublished($query)
    {
        $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * Локальный скоуп для еще не опубликованных записей
     *
     * @param $query
     * @return void
     */
    public function scopeUnpublished($query): void
    {
        $query->where('published_at', '>=', Carbon::now());
    }

    /**
     * Отношение к пользователю
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Отношение к тегам
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }
}
