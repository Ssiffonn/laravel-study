<?php

return [
    /**
     * Control if the seeder should create a user per role while seeding the data.
     */
    'create_users' => false,

    /**
     * Control if all the laratrust tables should be truncated before running the seeder.
     */
    'truncate_tables' => true,

    'roles_structure' => [
        'admin' => [
            'users' => 'c,r,u,d',
            'departments' => 'c,r,u,d',
            'positions' => 'c,r,u,d',
            'job_statuses' => 'c,r,u,d',
            'articles' => 'c,r,u,d',
            'roles' => 'c,r,u,d',
            'calendar' => 'c,r,u,d'
        ],
        'manager' => [
            'users' => 'c,r,u,d',
            'departments' => 'r,u',
            'positions' => 'r,u',
            'job_statuses' => 'r,u',
            'articles' => 'c,r,u,d',
            'roles' => 'r',
            'calendar' => 'c,r,u,d'
        ],
        'employee' => [
            'users' => 'r',
            'departments' => 'r',
            'positions' => 'r',
            'job_statuses' => 'r',
            'articles' => 'c,r,u,d',
            'calendar' => 'c,r,u'
        ]
    ],

    'permissions_map' => [
        'c' => 'create',
        'r' => 'read',
        'u' => 'update',
        'd' => 'delete'
    ],

    'ru_role' => [
        'admin' => 'админ',
        'manager' => 'менеджер',
        'employee' => 'сотрудник',
    ],

    'ru_permission' => [
        'create' => 'создать',
        'read' => 'просмотр',
        'update' => 'изменение',
        'delete' => 'удаление',
    ],

    'ru_perm' => [
        'users' => 'пользователи',
        'departments' => 'отделы',
        'positions' => 'должности',
        'job_statuses' => 'статусы',
        'articles' => 'статьи',
        'roles' => 'роли',
        'calendar' => 'события'
    ]
];
