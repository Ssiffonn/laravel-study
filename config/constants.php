<?php

return [
    'gender' => [
        'male' => 'male',
        'female' => 'female',
    ],
    'status' => [
        'active' => 'active',
        'inactive' => 'inactive',
    ],
];
