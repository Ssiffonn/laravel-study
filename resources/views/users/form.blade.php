@extends('adminlte::page')

@section('title', 'Пользователи')

@section('content')
    <form id='form' method="post" action="{{ $route }}">
        @csrf
        @method($method ?? null)
        <x-adminlte-card title="{{ $title }}" theme="dark" theme-mode="outline">
            <x-adminlte-input type="text" name="name" label="{{ __('user.name') }}" placeholder="John" 
            id="name" value="{{old('name') ?? $user->name ?? null}}">
                <x-slot name="prependSlot">
                    <div class="input-group-text">
                        <i class="fas fa-user text-lightblue"></i>
                    </div>
                </x-slot>
            </x-adminlte-input>

            <x-adminlte-input type="email" name="email" label="{{ __('user.email') }}" placeholder="mail@example.com" 
            id="email" value="{{ old('email') ?? $user->email ?? null }}">
                <x-slot name="prependSlot">
                    <div class="input-group-text">
                        <i class="fas fa-light fa-at text-lightblue"></i>
                    </div>
                </x-slot>
            </x-adminlte-input>

            <x-adminlte-select2 label="{{ __('user.department') }}" name="department_id" id='department' style="width: 100%">
                <option value="">{{ __('user.undefined') }}</option>
                @foreach ($departments as $row)
                    @if (isset($user) && $user->department != null && $user->department->name == $row->name || collect(old("department_id"))->contains($row->id))
                        <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                    @else
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-select2 label="{{ __('user.position') }}" name="position_id" id="position" style="width: 100%">
                <option value="">{{ __('user.undefined') }}</option>
                @foreach ($positions as $row)
                    @if (isset($user) && $user->position != null && $user->position->name == $row->name || collect(old("position_id"))->contains($row->id))
                        <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                    @else
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-select2 label="{{ __('user.status') }}" name="job_status_id" id="job_status" style="width: 100%">
                <option value="">{{ __('user.undefined') }}</option>
                @foreach ($jobStatuses as $row)
                    @if (isset($user) && $user->jobStatus != null && $user->jobStatus->name == $row->name || collect(old("job_status_id"))->contains($row->id))
                        <option value="{{ $row->id }}" selected>{{ $row->name }}</option>
                    @else
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-select2 label="{{ __('user.role') }}" name="role" id="role" style="width: 100%" >
                <option value="" selected disabled hidden></option>
                @foreach ($roles as $row)
                    @if (isset($user) && $user->hasRole($row->name) || collect(old("role"))->contains($row->id))
                        <option value="{{ $row->id }}" selected>{{ $row->display_name }}</option>
                    @else
                        <option value="{{ $row->id }}">{{ $row->display_name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>

            <label for="gender">{{ __('user.gender') }}</label>
            <div class="form-group gender">
                <div class="form-check">
                    <input id="male"
                    class="form-check-input" type="radio" name="gender_name"
                    value="{{ App\Models\User::GENDER['male']['name'] }}" checked>
                    <label class="form-check-label">{{ App\Models\User::GENDER['male']['name'] }}</label>
                </div>
                <div class="form-check">
                    <input id="female"
                    class="form-check-input" type="radio" name="gender_name"
                    value="{{ App\Models\User::GENDER['female']['name'] }}" {{ isset($user) && $user->gender_name
                        ==  App\Models\User::GENDER['female']['name'] ? 'checked="checked"' : '' }}>
                    <label class="form-check-label">{{ App\Models\User::GENDER['female']['name'] }}</label>
                </div>
            </div>

            <x-adminlte-input type="password" name="password" label="{{ __('user.password') }}" placeholder="password" 
            id="password">
                <x-slot name="prependSlot">
                    <div class="input-group-text">
                        <i class="fas fa-regular fa-key text-lightblue"></i>
                    </div>
                </x-slot>
            </x-adminlte-input>

            <x-adminlte-input type="password" name="password_confirmation" label="{{ __('user.password_confirmation') }}" placeholder="password" 
            id="password_confirmation">
                <x-slot name="prependSlot">
                    <div class="input-group-text">
                        <i class="fas fa-regular fa-key text-lightblue"></i>
                    </div>
                </x-slot>
            </x-adminlte-input>
            <x-slot name="footerSlot">
                <x-adminlte-button type="submit" theme="outline-success" label="{{ $btn }}" class="form-control" style="width: 200px"/>
            </x-slot>
        </x-adminlte-card>
    </form>
@endsection

@section('plugins.Select2', true)
