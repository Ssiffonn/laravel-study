@extends('adminlte::page')

@section('title', 'Пользователи')

@section('content')
    <x-adminlte-card theme="dark" theme-mode="outline">
        {{ __('user.mainTitle') }}
        @can('create', App\Models\User::class)
            <x-adminlte-button type="submit" id="addModal" label="{{ __('user.add') }}" theme="outline-success" icon="fa fa-plus"/>
        @endcan
        <x-adminlte-button id="show" theme="outline-primary" icon="fas fa-eye" label="{{__('user.myData')}}" data-id="{{ Auth::user()->id }}"/>
        <hr>
        {{ $dataTable->table() }}
    </x-adminlte-card>
    @include('buttons.delete')
@stop

@section('js')
    <script type="text/javascript" src="{{ asset('/js/user/userCrud.js') }}"></script>

    {{ $dataTable->scripts() }}
@stop

@section('plugins.Select2', true)
