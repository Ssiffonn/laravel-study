@extends('adminlte::page')

@section('title', 'Пользователи')

@section('content')
    <div class="d-flex bd-highlight mb-3">
        <div class="me-auto p-2 bd-highlight" style="width: 25%">
            <x-adminlte-card title="{{__('user.myData')}}" theme="dark" icon="fas fa-info-circle">
                <h5>{{__('user.name')}}</h5>{{ $user->name }}
                <p><h5>{{__('user.email')}}</h5>{{ $user->email }}</p>
                <p><h5>{{__('user.department')}}</h5>{{ $user->department != null ? $user->department->name : "Не выбрано" }}</p>
                <p><h5>{{__('user.position')}}</h5>{{ $user->position != null ? $user->position->name : "Не выбрано" }}</p>
                <p><h5>{{__('user.status')}}</h5>{{ $user->jobStatus != null ? $user->jobStatus->name : "Не выбрано" }}</p>
                <p><h5>{{__('user.role')}}</h5>{{ $user->roles != null ? $user->roles[0]->display_name : "Не выбрано" }}</p>
                <h5>{{__('user.gender')}}</h5>{{ $user->gender_name }}
            </x-adminlte-card>
        </div>
        <div class="p-2 bd-highlight" style="width: 75%;">
            <x-adminlte-card theme="dark" theme-mode="outline">
                <x-adminlte-button id="events" theme="outline-primary" label="{{__('user.events')}}"/>
                <x-adminlte-button id="log" theme="outline-primary" label="{{__('user.log')}}"/>
                <hr>
                <div id="event_table">
                    {{ $dataTable->table() }}
                </div>

                <div id="log_timeline" style="display: none">
                    <div class="timeline">
                        @foreach ($activity as $row)
                            <div class="time-label">
                                <span class="bg-green">{{ Carbon\Carbon::parse($row->created_at)->format("d.m.Y") }}</span>
                            </div>  
                            <div>
                                <i class="fas fa-user bg-blue"></i>
                                <div class="timeline-item">
                                    <span class="time"><i class="fas fa-clock">{{ Carbon\Carbon::parse($row->created_at)->format("H:i:s") }}</i></span>
                                    <h3 class="timeline-header">{{ $row->causer->name }} {{ App\Models\User::EVENT_NAME[$row->event] }} {{ $row->subject->name }}</h3>
                                    <div class="timeline-body">
                                        @foreach($row->properties['attributes'] as $key => $item)
                                            {{ App\Models\User::COLUMN[$key] }}:
                                            {{ !empty($row->properties['old']) ? $row->properties['old'][$key] . ' ➜' : '' }}
                                            {{ $item }}<br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>                  
                        @endforeach
                        <div>
                            <i class="fas fa-clock bg-gray"></i>
                        </div>
                    </div>
                </div>
            </x-adminlte-card>
        </div>
    </div>
@endsection

@section('js')
    <script src="{{ asset('/js/user/event_log.js') }}"></script>

    {{ $dataTable->scripts() }}
@endsection