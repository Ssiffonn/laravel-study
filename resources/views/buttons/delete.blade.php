<x-adminlte-modal theme='danger ' theme-mode="outline" v-centered='true' id="deleteModal" title="Удаление" size='md'>
    Точно удалить данные?
    <x-slot name="footerSlot">
        <x-adminlte-button theme="outline-success" label="Удалить" id='delete'/>
        <x-adminlte-button theme="outline-danger" label="Отмена" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>