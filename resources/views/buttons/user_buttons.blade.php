@role('admin')
    <x-adminlte-button id="show" theme="outline-primary" icon="fas fa-eye" data-id="{{ $id }}"/>
@endrole
@can('update', $model)
    <x-adminlte-button id="editModal" theme="outline-success" icon="fa fa-pen" data-toggle='modal' data-id="{{ $id }}" data-target='#modal'/>
@endcan
@can('delete', $model)
    <x-adminlte-button id="deleteBtn" theme="outline-danger" icon="fas fa-ban" data-id="{{ $id }}" data-toggle="modal" data-target='#deleteModal'/>
@endcan
