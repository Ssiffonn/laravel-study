@if (Session::has('flash_message'))
    <div class="alert alert-success {{ Session::get('flash_message_important') ? 'alert-important' : ''}} mt-2">
        @if (Session::get('flash_message_important'))   
            <button type="button" class="btn-close" aria-label="Close" data-dismiss="alert" aria-hidden="true"></button>
        @endif
        {{ session('flash_message') }}
    </div>
@endif