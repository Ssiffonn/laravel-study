@if ($errors->any())
    <ul class="alert alert-danger mt-2">
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif