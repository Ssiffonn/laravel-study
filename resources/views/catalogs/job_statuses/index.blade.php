@extends('adminlte::page')

@section('title', 'Статусы')

@section('content')
    <x-adminlte-card theme="dark" theme-mode="outline">
        {{ __('job_status.title') }}
        @can('create', App\Models\JobStatus::class)
            <x-adminlte-button label="{{ __('job_status.add') }}" theme="outline-success" icon="fa fa-plus" data-toggle='modal' data-target='#add'/>
        @endcan
        <hr>
        {{ $dataTable->table() }}
    </x-adminlte-card>
    @include('catalogs.template')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/js/references/crud.js') }}"></script>

    {{ $dataTable->scripts() }}
@endsection