<x-adminlte-modal theme='success' theme-mode="outline" v-centered='true' id="modal" class="myModal" title="" size='md'>
    <form action="" method="post" id="form">
        @csrf
        @if(isset($event) && Auth::user()->id != $event->creator_id && !Auth::user()->hasRole('admin'))
            <x-adminlte-input type="text" name="title" label="Заголовок события:" placeholder="Событие" 
            id="title" disabled="disabled"/>
            <x-adminlte-textarea name="description" id="description" placeholder="Описание" label="Описание:" disabled="disabled"/>
            <x-adminlte-select2 name='user_id' label='Пользователь для которого нужно создь ивент:' id="user_id" style="width: 100%" disabled="disabled">
                <option value="" selected disabled hidden></option>
                @foreach ($users as $user)
                    @if(isset($event) && $event->user_id == $user->id)
                        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                    @else
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>
        @else
            <x-adminlte-input type="text" name="title" label="Заголовок события:" placeholder="Событие" 
            id="title"/>
            <x-adminlte-textarea name="description" id="description" placeholder="Описание" label="Описание:"/>
            <x-adminlte-select2 name='user_id' label='Пользователь для которого нужно создь ивент:' id="user_id" style="width: 100%">
                <option value="" selected disabled hidden></option>
                @foreach ($users as $user)
                    @if(isset($event) && $event->user_id == $user->id)
                        <option value="{{ $user->id }}" selected>{{ $user->name }}</option>
                    @else
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>
        @endif
        
        @if(Request::is('*/edit'))
        <label for="work">Статус задачи:</label>
        <div class="form-group work">
            <div class="form-check">
                <input type="radio" name="work" id="work" value="work" class="form-check-input" {{ isset($event) && $event->status == 'work' ? 'checked="checked"' : ''}}>
                <label for="work">В работе</label>
            </div>
            <div class="form-check">
                <input type="radio" name="work" id="done" value="done" class="form-check-input" {{ isset($event) && $event->status == 'done' ? 'checked="checked"' : ''}}>
                <label for="done">Выполнена</label>
            </div>
        </div>
        @endif
        <x-slot name="footerSlot">
            <x-adminlte-button theme="outline-success" id='btn'/>
            @if(Request::is('*/edit'))
                @permission('calendar-delete')
                    @if(Auth::user()->id == $event->creator_id || Auth::user()->hasRole('admin'))
                        <x-adminlte-button theme="outline-danger" label="Удалить" id="delete"/>
                    @endif
                @endpermission
            @endif
        </x-slot>
    </form>
</x-adminlte-modal>
