@extends('adminlte::page')

@section('title', 'Календарь')

@section('content')
    <x-adminlte-card theme="dark" theme-mode="outline">
        <div id="calendar"></div>
    </x-adminlte-card>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('js/references/calendar/index.js') }}">
    </script>
@endsection

@section('plugins.Select2', true)
@section('plugins.Fullcalendar', true)
