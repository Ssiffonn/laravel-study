<x-adminlte-modal theme='success' theme-mode="outline" v-centered='true' id="add" title="Добавление" size='md'>
    <form id='addForm'>
        @csrf
        <x-adminlte-input name="name" label="Название:" id='addName'/>
    </form>
    <span class="error"></span>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="outline-success" label="Добавить" id='addBtn'/>
        <x-adminlte-button theme="outline-danger" label="Отмена" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

<x-adminlte-modal theme='success' theme-mode="outline" v-centered='true' id="modal" title="Изменение" size='md'>
    <form id='editForm'>
        @csrf
        <x-adminlte-input name="name" label="Название:" id='editName'/>
    </form>
    <span class="error"></span>
    <x-slot name="footerSlot">
        <x-adminlte-button theme="outline-success" label="Изменить" id='editBtn'/>
        <x-adminlte-button theme="outline-danger" label="Отмена" data-dismiss="modal"/>
    </x-slot>
</x-adminlte-modal>

@include('buttons.delete')