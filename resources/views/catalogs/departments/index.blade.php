@extends('adminlte::page')

@section('title', 'Отделы')

@section('content')
    <x-adminlte-card theme="dark" theme-mode="outline">
        {{ __('department.title') }}
        @can('create', App\Models\Department::class)
            <x-adminlte-button label="{{ __('department.add') }}" theme="outline-success" icon="fa fa-plus" data-toggle='modal' data-target='#add'/>
        @endcan
        <hr>
        {{ $dataTable->table() }}
    </x-adminlte-card>
    @include('catalogs.template')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/js/references/crud.js') }}"></script>
    
    {{ $dataTable->scripts() }}
@endsection

