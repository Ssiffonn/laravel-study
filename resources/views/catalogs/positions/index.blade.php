@extends('adminlte::page')

@section('title', 'Должности')

@section('content')
    <x-adminlte-card theme="dark" theme-mode="outline">
        {{ __('position.title') }}
        @can('create', App\Models\Position::class)
            <x-adminlte-button label="{{ __('position.add') }}" theme="outline-success" icon="fa fa-plus" data-toggle='modal' data-target='#add'/>
        @endcan
        <hr>
        {{ $dataTable->table() }}
    </x-adminlte-card>
    @include('catalogs.template')
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/js/references/crud.js') }}"></script>

    {{ $dataTable->scripts() }}
@endsection
