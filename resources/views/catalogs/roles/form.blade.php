@extends('adminlte::page')

@section('title', 'Роли')

@section('content')
    <form id="form" method="post" action="{{ $route }}">
        <x-adminlte-card title="{{ $title }} роли" theme="dark" theme-mode="outline">
            @csrf
            @method($method ?? null)
            <x-adminlte-input type="text" name="name" label="Название роли" placeholder="admin" 
            id="name" value="{{ old('name') ?? $role->name ?? null }}">
            </x-adminlte-input>

            <x-adminlte-input type="text" name="display_name" label="Отображаемое название роли" placeholder="Админ" 
            id="name" value="{{ old('display_name') ?? $role->display_name ?? null }}">
            </x-adminlte-input>

            @php
                $config = [
                    "placeholder" => "Выберите нужные опции...",
                    "allowClear" => true,
                    "closeOnSelect" => false,
                ];
            @endphp

            <x-adminlte-select2 label="Разрешить создание для:" name="perm[]" id='create' style="width: 100%" multiple :config="$config">
                @foreach ($permissions as $key => $permission)
                    <option value="{{ $key }}-create" {{ collect(old('perm'))->contains($key . '-create')
                    || isset($role) && $role->permissions->contains('name', $key . '-create')
                    ? 'selected="selected"'
                    : '' }}>{{ $permission }}</option>
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-select2 label="Разрешить просмотр для:" name="perm[]" id='read' style="width: 100%" multiple :config="$config">
                @foreach ($permissions as $key => $permission)
                    <option value="{{ $key }}-read" {{ collect(old('perm'))->contains($key . '-read')
                    ||isset($role) && $role->permissions->contains('name', $key . '-read')
                        ? 'selected="selected"'
                        : '' }}>{{ $permission }}</option>
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-select2 label="Разрешить изменение для:" name="perm[]" id='update' style="width: 100%" multiple :config="$config">
                @foreach ($permissions as $key => $permission)
                    <option value="{{ $key }}-update" {{ collect(old('perm'))->contains($key . '-update')
                    || isset($role) && $role->permissions->contains('name', $key . '-update')
                        ? 'selected="selected"'
                        : '' }}>{{ $permission }}</option>
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-select2 label="Разрешить удаление для:" name="perm[]" id='delete' style="width: 100%" multiple :config="$config">
                @foreach ($permissions as $key => $permission)
                    <option value="{{ $key }}-delete" {{ collect(old('perm'))->contains($key . '-delete')
                    || isset($role) && $role->permissions->contains('name', $key . '-delete')
                        ? 'selected="selected"'
                        : '' }}>{{ $permission }}</option>
                @endforeach
            </x-adminlte-select2>
            <x-slot name="footerSlot">
                <x-adminlte-button type="submit" theme="outline-success" label="{{ $btn }}" class="form-control" style="width: 100px"/>
            </x-slot>
        </x-adminlte-card>
    </form>
@endsection

@section('plugins.Select2', true)

