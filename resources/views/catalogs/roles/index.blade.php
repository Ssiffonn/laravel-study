@extends('adminlte::page')

@section('title', 'Роли')

@section('content')
    <x-adminlte-card theme="dark" theme-mode="outline">
        {{ __('department.title') }}
        @can('create', App\Models\Role::class)
            <x-adminlte-button id="addModal" label="{{ __('role.add') }}" theme="outline-success" icon="fa fa-plus"/>
        @endcan
        <hr>
        {{ $dataTable->table() }}
    </x-adminlte-card>
    <x-adminlte-modal theme='danger ' theme-mode="outline" v-centered='true' id="deleteModal" title="Удаление" size='md'>
        Точно удалить данные?
        <x-slot name="footerSlot">
            <x-adminlte-button theme="outline-success" label="Удалить" id='delete'/>
            <x-adminlte-button theme="outline-danger" label="Отмена" data-dismiss="modal"/>
        </x-slot>
    </x-adminlte-modal>
@endsection

@section('js')
    <script type="text/javascript" src="{{ asset('/js/references/roleCrud.js') }}"></script>
    
    {{ $dataTable->scripts() }}
@endsection
