@extends('adminlte::page')

@section('title', 'Статьи')

@section('content_header')
    <h1>{{ $article->title }}</h1>
    {{ __('article.published', ['published' => $article->published_at]) }}
@stop

@section('content')
    {{-- @if ($loggedUser == $article->user_id) --}}
    @unless (Auth::user()->hasRole('admin'))
        @isAbleToAndOwns('articles-update', $article)
            <a href={{ route('articles.edit', $article->id) }} class="btn btn-outline-dark">{{ __('article.edit') }}</a>
        @endOwns
    @else
    <a href={{ route('articles.edit', $article->id) }} class="btn btn-outline-dark">{{ __('article.edit') }}</a>

    @endunless
    {{-- @endif --}}
    <hr>
    <article>
        {{ $article->body }}
    </article>
    @unless ($article->tags->isEmpty())
        <h5>{{ __('article.tags') }}</h5>
        <ul>
            @foreach ($article->tags as $tag)
                <li><a href={{ route('articles.index', ['tag' => $tag->name]) }} class="list-group-item-action">{{ $tag->name }}</a></li>
            @endforeach
        </ul>
    @endunless
@endsection