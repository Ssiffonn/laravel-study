@extends('adminlte::page')

@section('title', 'Статьи')

@section('content_header')
    @include('flash::message')
    <h1>{{ __('article.mainTitle') }}</h1>
@stop

@section('content')
    @can('create', App\Models\Article::class)
        <a href={{ route('articles.create') }} class="btn btn-outline-dark">{{ __('article.add') }}</a>
    @endcan
    <hr>
    <div class="row">
        @foreach ($articles as $article)
            <div class="col-md-4">
                <x-adminlte-card title="{{ $article->title }}" theme="dark">
                    {{ __('article.creator', ['creator' => $article->user->name]) }}
                    <div class="body">{{ $article->body }}</div>
                    <a href="{{ route('articles.show', $article->id) }}" class="link-dark">{{ __('article.full') }}</a>
                </x-adminlte-card>
            </div>
        @endforeach
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script>
        $('div.alert').not('.alert-important').delay(3000).slideUp(300);
    </script>
@stop