@extends('adminlte::page')

@section('title', 'Статьи')

@section('content_header')
    <h1>{{ __('article.formTitle', ['title' => $title, 'article' => $article->title ?? '']) }}</h1>
@stop

@section('content')
    <form action="{{ $route }}" method="post">
        @csrf
        @method($method ?? null)
        <x-adminlte-card theme="dark" theme-mode="outline">
            <x-adminlte-input type="text" name="title" label="{{ __('article.title') }}" placeholder="Lorem  ipsum dolor" 
            id="title" value="{{ $article->title ?? old('title') }}"/>

            <x-adminlte-textarea name="body" id="body" placeholder="Lorem  ipsum dolor" label="{{ __('article.body') }}"
            cols="30" rows="10">
                {{ $article->body ?? old('body') }}
            </x-adminlte-textarea>

            @php
                $config = ['format' => 'DD.MM.YYYY HH:mm'];
            @endphp
            <x-adminlte-input-date name="published_at" value="{{ $published_at->format('d.m.Y H:i') }}" :config="$config" label="Дата публикации:"/>

            <x-adminlte-select2 id="tags" name="tags[]" label="{{ __('article.tags') }}" multiple="multiple">
                <x-slot name="prependSlot">
                    <div class="input-group-text bg-gradient-black">
                        <i class="fas fa-tag"></i>
                    </div>
                </x-slot>
                @foreach ($tags as $tag)
                    @if (isset($article) && $article->tags->contains($tag) || collect(old("tags"))->contains($tag->id))
                        <option value={{ $tag->id }} selected>{{ $tag->name }}</option>
                    @else
                        <option value={{ $tag->id }}>{{ $tag->name }}</option>
                    @endif
                @endforeach
            </x-adminlte-select2>

            <x-adminlte-button type="submit" label="{{ $submitButtonText }}" theme="outline-success" class="form-control"/>
        </x-adminlte-card>
    </form>
@endsection

@section('plugins.TempusDominusBs4', true)
@section('plugins.Select2', true)
