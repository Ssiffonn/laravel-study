<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Calendar\CalendarController;
use App\Http\Controllers\Calendar\EventsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\References\DepartmentController;
use App\Http\Controllers\References\JobStatusController;
use App\Http\Controllers\References\PositionController;
use App\Http\Controllers\References\RoleController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/home', [HomeController::class, 'index'])->middleware('auth')->name('home');
Route::resource('/articles', ArticleController::class)->middleware('auth');
Route::resource('/users', UserController::class)->middleware('auth');
Route::prefix('/catalogs')->group(function () {
    Route::resource('/departments', DepartmentController::class)->except(['create', 'edit', 'show'])
    ->middleware('auth');
    Route::resource('/positions', PositionController::class)->except(['create', 'edit', 'show'])->middleware('auth');
    Route::resource('/job_statuses', JobStatusController::class)->except(['create', 'edit', 'show'])
    ->middleware('auth');
    Route::resource('/roles', RoleController::class)->except(['show'])->middleware('auth');
    Route::resource('/tasks', CalendarController::class)->except(['show'])->middleware('auth');
    Route::get('tasks/events', [CalendarController::class, 'events'])->middleware('auth');
});
